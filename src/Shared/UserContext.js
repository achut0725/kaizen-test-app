import { createContext } from "react";

const UserContext = createContext({
    user: {
        name: "",
        email: "",
        number: "",
        password: "",
        status: false,
    },
})

UserContext.displayName = "UserContext";

export default UserContext;