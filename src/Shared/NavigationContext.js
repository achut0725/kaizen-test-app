import { createContext } from "react";

const NavigationContext = createContext({
    navigationBar: {
        navigation: "dashboard",
    },
});

NavigationContext.displayName = "NavigationContext";

export default NavigationContext;