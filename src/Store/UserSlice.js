import { createSlice } from "@reduxjs/toolkit";

const UserSlice = createSlice({
  name: "User",
  initialState: {
    users: [],
  },
  reducers: {
    addUser(state, action) {
        const newUser = action.payload;
        state.users.push(newUser);
    },
  },
});

export const { addUser } = UserSlice.actions;

export default UserSlice.reducer;
