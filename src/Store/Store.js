import { configureStore } from "@reduxjs/toolkit";
import AppointmentSlice from "./AppointmentSlice";
import UserSlice from "./UserSlice";

const store = configureStore({
  reducer: {
    appointments : AppointmentSlice,
    users: UserSlice,
  },
});

export default store;
