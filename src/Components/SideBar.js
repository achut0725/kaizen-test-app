import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import NavigationContext from "../Shared/NavigationContext";

function SideBar() {

  const navigate = useNavigate();

  // const [navigation, setNavigation] = useState("dashboard")

  const {navigationBar, setNavigationBar} = useContext(NavigationContext)

  const navigation = navigationBar.navigation
  console.log(navigation)

  const handleDashboardButtonClick =()=> {
    navigate("/")
    setNavigationBar({navigation: "dashboard"})
  }

  const handleProfileButtonClick =()=> {
    navigate("/profile")
    setNavigationBar({navigation: "profile"})
  }

  const handleMessagesButtonClick =()=> {
    navigate("/messages")
    setNavigationBar({navigation: "messages"})
  }

  const handleSettingsButtonClick =()=> {
    navigate("/settings")
    setNavigationBar({navigation: "settings"})
  }

  return (
    <>
      <div className="w-1/12 bg-blue-500 h-screen fixed left-0 top-0 py-16 pl-3 flex flex-col gap-4 border-r-2">
        <button className={`${navigation === "dashboard" ? "bg-white" : "bg-blue-500"} p-2 pr-5 rounded-l-xl`} onClick={()=> handleDashboardButtonClick()}>
          <img
            width="27"
            height="27"
            src="https://img.icons8.com/windows/32/calendar.png"
            alt="calendar"
          />
        </button>
        <button className={`${navigation === "profile" ? "bg-white" : "bg-blue-500"} p-2 pr-5 rounded-l-xl`} onClick={()=> handleProfileButtonClick()}>
          <img
            width="24"
            height="24"
            src="https://img.icons8.com/material-outlined/24/user-male-circle.png"
            alt="user-male-circle"
          />
        </button>
        <button className={`${navigation === "messages" ? "bg-white" : "bg-blue-500"} p-2 pr-5 rounded-l-xl`} onClick={()=> handleMessagesButtonClick()}>
          <img
            width="24"
            height="24"
            src="https://img.icons8.com/material-outlined/24/new-post.png"
            alt="new-post"
          />
        </button>
        <button className={`${navigation === "settings" ? "bg-white" : "bg-blue-500"} p-2 pr-5 rounded-l-xl`} onClick={()=> handleSettingsButtonClick()}>
          <img
            width="24"
            height="24"
            src="https://img.icons8.com/material-outlined/24/settings--v1.png"
            alt="settings--v1"
          />
        </button>
      </div>
    </>
  );
}

export default SideBar;
