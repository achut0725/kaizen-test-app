import React, { useContext, useState } from "react";
import UserContext from "../Shared/UserContext";
import { useDispatch } from "react-redux";
import { addUser } from "../Store/UserSlice";

function LoginPopup() {

  const {user, setUser} = useContext(UserContext);
  console.log(user)

  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [number, setNumber] = useState("");
  const [passwordTest, setPasswordTest] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit =(e)=> {
    e.preventDefault();

    if(
      name === "" ||
      email === "" ||
      number === "" ||
      password === ""
    ) {
      alert("Please fill in all the fields.");
      return;
    } else if(
      passwordTest !== password
    ) {
      alert("Password does not match.");
      return;
    }

    setUser({
      name: name,
      email: email,
      number: number,
      password: password,
      status: true,
    })

    const userDetails = {
      name: name,
      email: email,
      number: number,
      password: password,
      status: true,
    }

    dispatch(addUser(userDetails))
  }

  return (
    <div className="overlay fixed top-0 left-0 w-full h-full flex justify-center items-center bg-black bg-opacity-50 z-40">
      <div className="popup relative w-72 p-3 bg-white rounded-lg shadow-md text-center border-2 border-blue-500 bg-white/90">

        <form className="flex flex-col gap-2" onSubmit={handleSubmit}>
        <div className="flex flex-col justify-start text-left gap-1">
              <label>Name : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="text"
                value={name}
                placeholder="Full Name"
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Email : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="email"
                value={email}
                placeholder="Email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Phone Number : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="tel"
                value={number}
                placeholder="Phone Number"
                onChange={(e) => setNumber(e.target.value)}
              />
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Password : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="password"
                value={passwordTest}
                placeholder="Password"
                onChange={(e) => setPasswordTest(e.target.value)}
              />
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Confirm Password : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="password"
                value={password}
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>

            <button
              type="submit"
              className="bg-blue-500 w-20 p-1 px-2 rounded-lg text-white"
            >
              Submit
            </button>
          
        </form>
        
      </div>
    </div>
  );
}

export default LoginPopup;
