import React, { useState } from "react";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { addAppointment } from "../Store/AppointmentSlice";
import store from "../Store/Store";

function NewAppointment({ data, onClose }) {
  const dispatch = useDispatch();

  const appointmentsFromStore = useSelector(
    (store) => store.appointments.appointments
  );

  const calendarDate = moment(data.date).format("YYYY-MM-DD");

  const [selectedDoctor, setSelectedDoctor] = useState(data.doctors[0]);
  const [selectedDate, setSelectedDate] = useState(calendarDate);
  console.log(selectedDate);
  const [selectedTimeSlot, setSelectedTimeSlot] = useState(data.timeSlots[0]);
  const [selectedEndTime, setSelectedEndTime] = useState(data.endTime[0]);
  const [userName, setUserName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const [submitted, setSubmitted] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      selectedDoctor === "" ||
      selectedDate === "" ||
      selectedTimeSlot === "" ||
      selectedEndTime === "" ||
      userName === "" ||
      phoneNumber === ""
    ) {
      alert("Please fill in all the fields");
      return;
    }

    const appointment = {
      doctor: selectedDoctor,
      selectedDate: selectedDate,
      timeSlot: selectedTimeSlot,
      endTime: selectedEndTime,
      userName,
      phoneNumber,
    };

    // console.log(appointment);

    const existingAppointment = appointmentsFromStore.find(
      (appointmentFromStore) =>
        appointmentFromStore.selectedDate === appointment.selectedDate &&
        appointmentFromStore.doctor === appointment.doctor &&
        (appointmentFromStore.timeSlot === appointment.timeSlot ||
          appointmentFromStore.endTime === appointment.endTime)
    );

    if (existingAppointment) {
      setSubmitted(false);
      alert("this time period has already been taken");
    } else if (!existingAppointment) {
      dispatch(addAppointment(appointment));
      //   console.log("dispatched");
      setSubmitted(true);
      setTimeout(() => {
        onClose();
      }, 1000);
    }
  };

  const onSubmitButtonClick = () => {
    setTimeout(() => {
      if (submitted) {
        onClose();
      }
    }, 1000);
  };

  return (
    <>
      <div className="overlay fixed top-0 left-0 w-full h-full flex justify-center items-center bg-black bg-opacity-50 z-40">
        <div className="popup relative w-96 p-3 bg-white rounded-lg shadow-md text-center border-2 border-blue-500 bg-white/90">
          <form className="flex flex-col gap-2" onSubmit={handleSubmit}>
            <div className="flex flex-col justify-start text-left gap-1">
              <div className="flex justify-between">
                <label>Doctor : </label>
                <button onClick={onClose}>
                  <img
                    className="bg-red-300 rounded-md mb-2"
                    width="24"
                    height="24"
                    src="https://img.icons8.com/material-outlined/24/delete-sign.png"
                    alt="delete-sign"
                  />
                </button>
              </div>

              <select
                className="p-2 border-2 border-blue-300 rounded-md"
                value={selectedDoctor}
                onChange={(e) => setSelectedDoctor(e.target.value)}
              >
                {data.doctors.map((doctor) => {
                  return (
                    <option key={doctor} className="p-2">
                      {doctor}
                    </option>
                  );
                })}
              </select>
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Select a date</label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="date"
                value={selectedDate}
                required
                onChange={(e) => setSelectedDate(e.target.value)}
              />
            </div>

            <div className="flex flex-row justify-between">
              <div className="flex flex-col justify-start text-left gap-1">
                <label>Start Time : </label>
                <select
                  className="p-2 border-2 border-blue-300 rounded-md w-40"
                  value={selectedTimeSlot}
                  onChange={(e) => setSelectedTimeSlot(e.target.value)}
                >
                  {data.timeSlots.map((time) => {
                    return (
                      <option key={time} className="p-2">
                        {time}
                      </option>
                    );
                  })}
                </select>
              </div>

              <div className="flex flex-col justify-start text-left gap-1">
                <label>End Time : </label>
                <select
                  className="p-2 border-2 border-blue-300 rounded-md w-40"
                  value={selectedEndTime}
                  onChange={(e) => setSelectedEndTime(e.target.value)}
                >
                  {data.endTime.map((time) => {
                    return (
                      <option key={time} className="p-2">
                        {time}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Name : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="text"
                value={userName}
                placeholder="Full Name"
                onChange={(e) => setUserName(e.target.value)}
              />
            </div>

            <div className="flex flex-col justify-start text-left gap-1">
              <label>Phone Number : </label>
              <input
                className="p-2 border-2 border-blue-300 rounded-md outline-none"
                type="tel"
                value={phoneNumber}
                placeholder="Phone Number"
                onChange={(e) => setPhoneNumber(e.target.value)}
              />
            </div>

            <button
              type="submit"
              className="bg-blue-500 w-20 p-1 px-2 rounded-lg text-white"
              onClick={() => onSubmitButtonClick()}
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </>
  );
}

export default NewAppointment;
