import moment from "moment";
import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";

import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import NewAppointment from "./NewAppointment";
import UserContext from "../Shared/UserContext";

function Head({data, dateToDashboard}) {

  const {user, setUser} = useContext(UserContext);
  console.log(user)

  const [date, setDate] = useState(new Date());
  // console.log(date);

  const handlePreviousDay = () => {
    const previousDate = new Date(date);
    previousDate.setDate(date.getDate() - 1);
    setDate(previousDate);
    dateToDashboard(previousDate);
  };

  const handleNextDay = () => {
    const nextDate = new Date(date);
    nextDate.setDate(date.getDate() + 1);
    setDate(nextDate);
    dateToDashboard(nextDate);
  };

  const formatedDate = moment(date).format("MMM Do YY");
//   console.log(formatedDate);

  const [isDateClicked, setIsDateClicked] = useState(false);

  const handleDateClick = () => {
    if (isDateClicked === false) {
      setIsDateClicked(true);
    } else {
      setIsDateClicked(false);
    }
  };

  useEffect(() => {
    setIsDateClicked(false);
  }, [date]);

  const handleDateChange = (newDate) => {
    setDate(newDate);
    dateToDashboard(newDate);
  };

  const [appointmentPopup, setAppointmentPopup] = useState(false);

  const handleAppointmentPopup =()=> {
    if(appointmentPopup === false) {
        setAppointmentPopup(true)
    } else {
        setAppointmentPopup(false);
    }
  }

  const handlePopupClose =()=> {
    setAppointmentPopup(false);
    console.log("close")
  }

  const navigate = useNavigate();
  
  return (
    <>
      <div className="bg-blue-200 w-auto border-b-2 border-blue-500">
        <div className="border-b-2 border-blue-500 p-3 flex justify-between">
          <h1>Appointments</h1>

          <div className="w-40 flex justify-between">
            <img
              width="24"
              height=""
              src="https://img.icons8.com/material-outlined/24/settings--v1.png"
              alt="settings--v1"
            />

            <img
              width="24"
              height="24"
              src="https://img.icons8.com/puffy/32/experimental-appointment-reminders-puffy.png"
              alt="experimental-appointment-reminders-puffy"
            />

            <div className="flex gap-2 bg-blue-400 rounded-lg px-1" onClick={()=> navigate("/profile")}>
              <img
                width="24" 
                height="24"
                src="https://img.icons8.com/material-outlined/24/user-male-circle.png"
                alt="user-male-circle"
              />

              {user.name ? <h1>{user.name}</h1> : <h1>Profile</h1>}
            </div>
          </div>
        </div>

        <div className="p-2 flex justify-between">
          <div className="flex gap-1">
            <button
              className="bg-blue-400 p-1 rounded-lg"
              onClick={handlePreviousDay}
            >
              <img
                width="24"
                height="24"
                src="https://img.icons8.com/material-outlined/24/back--v1.png"
                alt="back--v1"
              />
            </button>

            <button
              onClick={() => handleDateClick()}
              className="bg-blue-400 p-1 rounded-lg"
            >
              {formatedDate}
            </button>

            <button
              className="bg-blue-400 p-1 rounded-lg"
              onClick={handleNextDay}
            >
              <img
                width="24"
                height="24"
                src="https://img.icons8.com/material-outlined/24/forward.png"
                alt="forward"
              />
            </button>
          </div>

          <button className="bg-blue-500 p-1 px-2 rounded-lg text-white" onClick={()=> {handleAppointmentPopup()}}>
            + New Appointment
          </button>

        </div>

        {isDateClicked && (
          <Calendar
            onChange={handleDateChange}
            value={date}
            className="bg-blue-200 rounded-lg absolute"
          />
        )}

        {appointmentPopup && <NewAppointment data={data} onClose={handlePopupClose}/>}
      </div>
    </>
  );
}

export default Head;
