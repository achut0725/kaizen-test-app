import React, {useContext, useEffect, useState} from "react";

import Head from "./Head";
import Grid from "./Grid";
import LoginPopup from "./LoginPopup";
import UserContext from "../Shared/UserContext";
import { useSelector } from "react-redux";
import store from "../Store/Store";

function DashBoard() {

  const [selectedDate, setSelectedDate] = useState(new Date());
  // console.log(selectedDate)

  const handleDateToDashboard = (date) => {
    setSelectedDate(date);
  };

  const userDetails = useSelector((store)=> store.users)
  console.log(userDetails)

  const {user, setUser} = useContext(UserContext);
  console.log(user)

  const [showPopup, setShowPopup] = useState(true);

  useEffect(() => {
    if (user.status === true) {
      setShowPopup(false);
    }
  console.log(user)
  }, [user.status]);

  const data = {
    doctors: ["Dr. Anderson", "Dr. Patel", "Dr. Lewis"],
    timeSlots: [
      "10:00 AM",
      "11:00 AM",
      "12:00 PM",
      "01:00 PM",
      "02:00 PM",
      "03:00 PM",
      "04:00 PM",
      "05:00 PM",
    ],
    endTime: [
      "10:45 AM",
      "11:45 AM",
      "12:45 PM",
      "01:45 PM",
      "02:45 PM",
      "03:45 PM",
      "04:45 PM",
      "05:45 PM",
    ],
    date: selectedDate,
  };

  // console.log(data)

  return (
    <>
      <div className="w-11/12 ml-auto">
        <Head data={data} dateToDashboard={handleDateToDashboard} /> 
        <Grid data={data} />
      </div>
      {showPopup && <LoginPopup />}
    </>
  );
}

export default DashBoard;
