import React from "react";
import { useSelector } from "react-redux";

const Grid = ({ data }) => {
  const doctors = data.doctors;
  const timeSlots = data.timeSlots;
  const appointments = useSelector((store) => store.appointments.appointments);
  console.log(appointments)

  // Create a mapping of appointments by time slot
  const appointmentMap = {};
  timeSlots.forEach((timeSlot) => {
    appointmentMap[timeSlot] = {};
  });

  appointments.forEach((appointment) => {
    const { timeSlot, doctor } = appointment;

    if (!appointmentMap[timeSlot][doctor]) {
      appointmentMap[timeSlot][doctor] = [];
    }

    appointmentMap[timeSlot][doctor].push(appointment);
  });

  return (
    <div className="grid grid-cols-4 gap-4">
      {/* Render doctor names at the top */}
      <div className="col-span-1 bg-gray-200 text-center py-2">Time</div>
      {doctors.map((doctor, index) => (
        <div key={index} className="col-span-1 bg-gray-200 text-center py-2">
          {doctor}
        </div>
      ))}

      {/* Render time slots on the left */}
      {timeSlots.map((time, index) => (
        <React.Fragment key={index}>
          <div className="col-span-1 bg-gray-200 text-center py-2">{time}</div>
          {doctors.map((doctor, idx) => (
            <div key={idx} className="col-span-1 bg-white">
              {appointmentMap[time]?.[doctor]?.map((appointment) => (
                <div key={appointment.userName} className="flex justify-between border-2 p-2 h-auto">
                  <h1>{appointment.userName}</h1>
                  <h1>{appointment.phoneNumber}</h1>
                </div>
              ))}
            </div>
          ))}
        </React.Fragment>
      ))}
    </div>
  );
};

export default Grid;
