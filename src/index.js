import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import { Provider } from "react-redux";

import SideBar from "./Components/SideBar";
import DashBoard from "./Components/DashBoard";
import store from "./Store/Store";
import Profile from "./Components/Profile";
import Messages from "./Components/Messages";
import Settings from "./Components/Settings";
import NavigationContext from "./Shared/NavigationContext";
import UserContext from "./Shared/UserContext";

const AppLayout = () => {

  const [navigationBar, setNavigationBar] = useState({
    navigation: "dashboard",
  });

  const [user, setUser] = useState({
    name: "",
    email: "",
    number: "",
    password: "",
    status: false,
  });

  return (
    <>
      <Provider store={store}>
        <NavigationContext.Provider
          value={{
            navigationBar: navigationBar,
            setNavigationBar: setNavigationBar,
          }}
        >
          <UserContext.Provider value={{
            user: user,
            setUser: setUser,
          }}>
            <SideBar />
            <Outlet />
          </UserContext.Provider>
        </NavigationContext.Provider>
      </Provider>
    </>
  );
};

const appRouter = createBrowserRouter([
  {
    path: "/",
    element: <AppLayout />,
    children: [
      {
        path: "/",
        element: <DashBoard />,
      },
      {
        path: "profile",
        element: <Profile />,
      },
      {
        path: "messages",
        element: <Messages />,
      },
      {
        path: "settings",
        element: <Settings />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <RouterProvider router={appRouter} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
